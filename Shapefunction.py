import numpy as np
def Shapefunc(xi, eta):      

    # shape functions
    N = np.zeros(4)   # N = [N1,N2,N3,N4]

    N[0]=(0.25 * (1 - xi) * (1 - eta))
    N[1]=(0.25 * (1 + xi) * (1 - eta))
    N[2]=(0.25 * (1 + xi) * (1 + eta))
    N[3]=(0.25 * (1 - xi) * (1 + eta))    
        
    # Shape function derivatives
    dNdxi = np.zeros(4) #Initialization
    dNdxi[0] = (-0.25 * (1 - eta))
    dNdxi[1] = (0.25 * (1 - eta))
    dNdxi[2] = (0.25 * (1 + eta))
    dNdxi[3] = (-0.25 * (1 + eta))

    dNdeta = np.zeros(4)#Initialization
    dNdeta[0] = (-0.25 * (1 - xi))
    dNdeta[1] = (-0.25 * (1 + xi))
    dNdeta[2] = (0.25 * (1 + xi))
    dNdeta[3] = (0.25 * (1 - xi))

    return N, dNdxi, dNdeta


def ShapefuncDerivative(nnel, dNdxi, dNdeta, invjacob):
    
    dNdx = np.zeros(4)
    dNdy = np.zeros(4)

    for i in range(nnel):
        dNdx[i] = invjacob[0, 0] * dNdxi[i] + invjacob[0, 1] * dNdeta[i]
        dNdy[i] = invjacob[1, 0] * dNdxi[i] + invjacob[1, 1] * dNdeta[i]

    return dNdx, dNdy


def ShapeDerivativeForInitialStressMatrix(nnel, dNdx, dNdy, shape):

    dN = np.zeros((2,20))

    for i in range(nnel):
        N1 = 5 * i     #i * 5         
        N2 = N1 + 1
        N3 = N2 + 1
        N4 = N3 + 1
        N5 = N4 + 1
        
        dN[0,N3]=dNdx[i]
        dN[1,N3]=dNdy[i]
        dN[0,N4]=0
        dN[1,N5]=0        
        
    return dN
               
#z = y = ShapeDerivativeForInitialStressMatrix(4, [1, 1, 1, 1], [1, 1, 1, 1], 1 )             
#print(z) 
    
#z = Shapefunctions(2, 5)
#print(z)