#========================================================================================
#Function compute kinematic stiffness matrix associated with the shear components
#Variables:
#     Input: dNdx = shape function derivative w.r.t x
#            dNdy = shape function derivative w.r.t y
#            nodes = elemental nodes i.e. Q4 element
#            N = Shape function
#Function returns: kinematic stiffness matrix 
#========================================================================================
import numpy as np

def KinematicShear(nodes, dNdx, dNdy, N):
    B_Shear = np.zeros((2,20)) #initializing kinematic matrix
    for i in range(nodes):
        a = i * 5 
        b = a + 1
        c = b + 1
        d = c + 1
        e = d + 1

        B_Shear[0, c] = dNdx[i]
        B_Shear[1, c] = dNdy[i]
        B_Shear[0, d] = -N[i]        
        B_Shear[1, e] = -N[i]

    return B_Shear