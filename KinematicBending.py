#=========================================================================================================================
#Function compute kinematic stiffness matrix associated with the combined effect of linear inplane and linear bending components
#Variables:
#     Input: dNdx = shape function derivative w.r.t x
#            dNdy = shape function derivative w.r.t y
#            nodes = elemental nodes i.e. Q4 element
# Function returns: kinematic stiffness matrix  
#=========================================================================================================================
import numpy as np
def KinematicBending(nodes, dNdx, dNdy):
    BMatrix = np.zeros((6, 20))
    for i in range(nodes):
        B1 = i * 5 
        B2 = B1 + 1
        B3 = B2 + 1
        B4 = B3 + 1
        B5 = B4 + 1

        BMatrix[0, B1] = dNdx[i]
        BMatrix[1, B2] = dNdy[i]
        BMatrix[2, B1] = dNdy[i]
        BMatrix[2, B2] = dNdx[i]
        BMatrix[3, B4] = -dNdx[i]
        BMatrix[4, B5] = -dNdy[i]
        BMatrix[5, B4] = -dNdy[i]
        BMatrix[5, B5] = -dNdx[i]

    return BMatrix