def InitialStressVector(dshapedx, dshapedy, u_p, v_p, w_p, E, t, nu):

   
    dudx = dshapedx[0] * u_p[0] + dshapedx[1] * u_p[1] + dshapedx[2] * u_p[2] + dshapedx[3] * u_p[3]
    dudy = dshapedy[0] * u_p[0] + dshapedy[1] * u_p[1] + dshapedy[2] * u_p[2] + dshapedy[3] * u_p[3]
    dvdx = dshapedx[0] * v_p[0] + dshapedx[1] * v_p[1] + dshapedx[2] * v_p[2] + dshapedx[3] * v_p[3]
    dvdy = dshapedy[0] * v_p[0] + dshapedy[1] * v_p[1] + dshapedy[2] * v_p[2] + dshapedy[3] * v_p[3]
    dwdx = dshapedx[0] * w_p[0] + dshapedx[1] * w_p[1] + dshapedx[2] * w_p[2] + dshapedx[3] * w_p[3]
    dwdy = dshapedy[0] * w_p[0] + dshapedy[1] * w_p[1] + dshapedy[2] * w_p[2] + dshapedy[3] * w_p[3]



    return pb[0, 0] = E * t / (1 - nu ** 2) * (dudx + 0.5 * (dwdx) ** 2) + E * t * nu / (1 - nu ** 2) * (dvdy + 0.5 * (dwdy) ** 2),
           pb[0, 1] = E * t * 0.5 / (1 + nu) * (dudy + dvdx + dwdx * dwdy),
           pb[1, 0] = E * t * 0.5 / (1 + nu) * (dudy + dvdx + dwdx * dwdy),
           pb[1, 1] = E * t * nu / (1 - nu ** 2) * (dudx + 0.5 * (dwdx) ** 2) + E * t / (1 - nu ** 2) * (dvdy + 0.5 * (dwdy) ** 2)

#InitialStressVector([1,1,1,1], [2,5,6,7], [1,2,3,4], [5,4,3,2], [1,5,9,1], 210, 10, 0.3)
#print(pb)