#=============================================================================================
#Function compute kinematic stiffness matrix associated with the non linear inplane components
#Variables:
#     Input: dNdx = shape function derivative w.r.t x
#            dNdy = shape function derivative w.r.t y
#            nodes = elemental nodes i.e. Q4 element
#            w = displacement along z direction
#Function returns: kinematic stiffness matrix 
#=============================================================================================
import numpy as np
def KinematicNonlinear(nodes, dNdx, dNdy, w):

    dwdx = dNdx[0] * w[0] + dNdx[1] * w[1] + dNdx[2] * w[2] + dNdx[3] * w[3]
    dwdy = dNdy[0] * w[0] + dNdy[1] * w[1] + dNdy[2] * w[2] + dNdy[3] * w[3]    

    KineMat = np.zeros((6,20)) #initializing kinematic matrix    
    for i in range(nodes):
        a = i * 5 
        b = a + 1
        c = b + 1
        d = c + 1
        e = d + 1

        KineMat[0, c] = dNdx[i]*dwdx
        KineMat[1, c] = dNdy[i]*dwdy
        KineMat[2, c] = dwdy*dNdx[i] + dwdx*dNdy[i]
        
    return KineMat
    





