import numpy as np
import matplotlib.pyplot as plt
def Jacobian(nnel, dshapedxi, dshapedeta, xcoord, ycoord):


    jacobian = np.zeros([2, 2])
    
    #print(jacobian)
    
    
    for i in range(1, nnel):
        jacobian[0, 0] = jacobian[0, 0] + dshapedxi[i]*xcoord[i]
        jacobian[0, 1] = jacobian[0, 1] + dshapedxi[i]*ycoord[i]
        jacobian[1, 0] = jacobian[1, 0] + dshapedeta[i]*xcoord[i]
        jacobian[1, 1] = jacobian[1, 1] + dshapedeta[i]*ycoord[i] 
        

    
    return jacobian


    #detjacobian = np.linalg.det(jacobian)    # Determinant of Jacobian matrix 
    #invjacobian = np.linalg.inv(jacobian)    # Inverse of Jacobian matrix
    
   
#anoop = Jacobian(2,[1, 2, 3],[1, 2, 3],[1, 2, 3],[1, 2, 3])  
#print(anoop)

